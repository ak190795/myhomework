let userNumber = parseInt(prompt('please enter a number'));

while (isNaN(userNumber) || userNumber <= 0 || userNumber === null) {
    userNumber = parseInt(prompt('please enter a positive number'));
}

if (userNumber >= 5) {
    for (let i = 1; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i)
        }
    }
} else {
    console.log('Sorry, no numbers')
}






