


const firstNumMes = 'enter first num'
const correctFirstNumMes = 'enter correct first num'
const secondNumMes = 'enter second num'
const correctSecondNumMes = 'enter correct second num'

function userInput(enterMes, enterCorrectMes) {
    let userNumber = prompt(enterMes);
    while (isNaN(+userNumber) || +userNumber === 0) {
        userNumber = prompt(enterCorrectMes, userNumber);
    }
    return parseFloat(userNumber)
}

let firstNum = userInput(firstNumMes, correctFirstNumMes)
let secondNum = userInput(secondNumMes, correctSecondNumMes)
let enterOperation = prompt('enter operation');
while (enterOperation !== "*" && enterOperation !== "+" && enterOperation !== "-" && enterOperation !== "/") {
    enterOperation = prompt('enter correct operation', enterOperation);
}

function performMathAction(firstNum, secondNum, operation) {
    let result;
    switch (operation) {
        case "*":
            return result = firstNum * secondNum;
        case "/":
            return result = firstNum / secondNum;
        case "+":
            return result = firstNum + secondNum;
        case "-":
            return result = firstNum - secondNum;
    }
}

console.log(performMathAction(firstNum, secondNum, enterOperation))



