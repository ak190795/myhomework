let tabs = document.querySelectorAll('.tabs-title')
let tabsContent = document.querySelectorAll('.tabs-content')

tabs.forEach(element => element.addEventListener('click', function () {
        tabs.forEach(elem => elem.classList.remove('active'))
        tabsContent.forEach(elem => elem.style.display = 'none')
        element.classList.add('active')
        document.getElementById(this.dataset.toggle).style.display = 'block' 
    })
)

tabs[0].click()