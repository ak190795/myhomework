const inputRef = document.querySelector(".input")
const spanText = document.createElement("span")
const formRef = document.querySelector(".form")
const spanX = document.createElement("span")
const divRef = document.createElement("div")
spanX.classList = ("XStyle")
spanX.textContent = "X"
inputRef.addEventListener("blur", onImputBlur)
spanX.addEventListener("click", clickSpanX)
function clickSpanX() {
    inputRef.value = 0
divRef.remove()
}
function onImputBlur() {

    if (inputRef.value > 0) {
        formRef.before(divRef)
        spanText.textContent = `Текущая цена: ${inputRef.value}`
        divRef.append(spanText)
        divRef.append(spanX)
        inputRef.classList.add("green")
        inputRef.classList.remove("red")
    }
    if (inputRef.value < 0) {
       inputRef.classList.add("red")
        divRef.remove()
        spanText.textContent = "Please enter correct price"
        formRef.after(spanText)
    }
}
