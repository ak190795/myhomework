const themeBtn = document.querySelector('.theme')
const icons = document.querySelectorAll('path')
const themeStyle = document.createElement('link')

themeStyle.setAttribute('rel', 'stylesheet')
themeStyle.setAttribute('href','css/darkStyle.css')

if(localStorage.getItem('theme') === 'dark') {
    document.head.append(themeStyle)
    themeBtn.textContent = 'Light theme'
    icons.forEach(element=> element.setAttribute('fill', '#5a7bb0'))
}

themeBtn.addEventListener('click', () => {
    if (localStorage.getItem('theme') === 'light' || localStorage.getItem('theme') === null) {
        localStorage.setItem('theme', 'dark')
        icons.forEach(element=> element.setAttribute('fill', '#5a7bb0'))
        document.head.append(themeStyle)
        themeBtn.textContent = 'Light theme'
    } else {
        localStorage.setItem('theme', 'light')
        themeBtn.textContent = 'Dark theme'
        icons.forEach(element=> element.setAttribute('fill', 'white'))
        if (themeStyle) themeStyle.remove()
    }


})
console.log(localStorage.getItem('theme'))